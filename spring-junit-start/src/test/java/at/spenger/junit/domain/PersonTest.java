package at.spenger.junit.domain;

import static org.junit.Assert.*;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import at.spenger.junit.ApplicationTestAbstract;

public class PersonTest extends ApplicationTestAbstract {

	private Person p;
	private static final String P_DATE = "1950.09.27";
	
	@Before
	public void setUp() throws Exception {
		p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
	}

	@Test
	public void testGetBirthdayString() {
		String s = p.getBirthdayString();
		assertEquals(P_DATE, s);
	}

	@Test
	public void testAge(){
		Instant fixedInstance = Instant.parse("2010-01-02T11:00:00Z");
		Clock fixedClock = Clock.fixed(fixedInstance, ZoneId.of("Europe/Vienna"));
		
		p = new Person("Sam", "Lake", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
		ReflectionTestUtils.setField(p, "clock", fixedClock);
		int age = p.getAge();
		assertEquals(59, age);
	}
}

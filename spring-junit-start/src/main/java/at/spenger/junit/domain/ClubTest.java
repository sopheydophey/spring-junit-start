package at.spenger.junit.domain;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import at.spenger.junit.domain.Person.Sex;

public class ClubTest {

	Club a = new Club();
	
	@Test
	public void testEnter() {
		final LocalDate ld = LocalDate.of(1979, 6, 17);
		final LocalDate ld2 = LocalDate.of(1987, 9, 4);
		final LocalDate ld3 = LocalDate.of(1982, 2, 26);
		final LocalDate ld4 = LocalDate.of(1991, 11, 30);
		Sex sf = Sex.FEMALE;
		Sex sm = Sex.MALE;
		
		a.enter(new Person("John", "Smith", ld , sm));
		a.enter(new Person("Zara", "McBurrow", ld2 , sf));
		a.enter(new Person("Robert", "Newman", ld3 , sm));
		a.enter(new Person("Lauren", "Kelter", ld4 , sf));
	}

	@Test
	public void testNumberOf() {
		Club a = new Club();
		
		int erg = a.numberOf();
		int exp = 0;
		assertEquals(exp, erg);
	}
	
	@Test
	public void testAverageAge(){
		
		testEnter();
		a.averageAge();
		
	}
	
	@Test
	public void testSort(){
		testEnter();
		a.sortPeople();	
	}

	@Test
	public void testDelete(){
		Person p = new Person("George", "Simpson", LocalDate.of(1995, 8, 29), Sex.MALE);
		a.enter(p);
		testEnter();
		a.delete(p);
	}
	
	@Test
	public void testDifferenceDays(){
		testEnter();
		a.differenceDays();
	}
	
	
	@Test
	public void testRelation(){
		testEnter();
		a.relationMaleToFemale();
	}
	
	//--------
	
	@Test
	public void testSortCollect(){
		testEnter();
		a.sortName();
		List<Person> l = a.getPersons();
		System.out.println(l.toString());
		
		assertTrue(l.get(0).getLastName().equals("Kelter"));
		assertTrue(l.get(1).getLastName().equals("McBurrow"));
		assertTrue(l.get(2).getLastName().equals("Newman"));
		assertTrue(l.get(3).getLastName().equals("Smith"));
		
	}
	
	
	//---- JAVA8 TESTS -----------
	
	@Test
	public void testAverageAgeJava8(){
		testEnter();
		a.averageAgeJava8();
	}
	
	/*
	@Test
	public void testSortJava8(){
		testEnter();
		a.sortNameAge();
		
		List<Person> l = a.getPersons();
		assertTrue(l.get(0).getLastName().equals("Kelter"));
		assertTrue(l.get(1).getLastName().equals("McBurrow"));
		assertTrue(l.get(2).getLastName().equals("Newman"));
		assertTrue(l.get(3).getLastName().equals("Smith"));

	}
	*/
	
	@Test
	public void testGroupByJava8(){
		testEnter();
		Map <Object, Long> m = a.groupBirthYear();
		
		List<Person> l = a.getPersons();
		for(Person p : l){
			assertTrue(m.get(p.getBirthYear()).equals(1));
		}
	}
	
	@Test
	public void testDeleteJava8(){
		Person p = new Person("George", "Simpson", LocalDate.of(1995, 8, 29), Sex.MALE);
		a.enter(p);
		testEnter();
		a.deleteJava8(p);
		
		
		List<Person> l = a.getPersons();
		assertTrue(l.get(3).getLastName().equals("Smith"));
		assertTrue(l.get(1).getLastName().equals("McBurrow"));
		assertTrue(l.get(2).getLastName().equals("Newman"));
		assertTrue(l.get(0).getLastName().equals("Kelter"));
	}
	
	public void testBornInJava8(){
		testEnter();
		a.bornInJava8();
	}
	
}
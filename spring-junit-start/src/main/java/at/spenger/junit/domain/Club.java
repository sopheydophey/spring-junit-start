package at.spenger.junit.domain;

import java.time.LocalDate;
import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import at.spenger.junit.domain.Person.Sex;

public class Club {
	private List<Person> l;
	
	public Club() {
		l = new ArrayList<>();
	}
	
	public boolean enter(Person p) {
		if (p == null) {
			throw new IllegalArgumentException("person is null!");
		}
		return l.add(p);
	}
	
	public int numberOf() {
		return l.size();
	}
	
	public List<Person> getPersons() {
		return Collections.unmodifiableList(l);
	}

	public int averageAge(){
		int totalage = 0;
		int avg = 0;
		int no = 0;
		
		for(Person p : l){
			Calendar birthDay = Calendar.getInstance();
			birthDay.set(Calendar.YEAR, p.getBirthday().getYear());
			birthDay.set(Calendar.MONTH, p.getBirthday().getMonthValue());
			birthDay.set(Calendar.DATE, p.getBirthday().getDayOfMonth());
			 
			long currentTime = System.currentTimeMillis();
			Calendar currentDay = Calendar.getInstance();
			currentDay.setTimeInMillis(currentTime);
			 
			//Get difference between years
			int years = currentDay.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
			
			totalage +=years;
			no++;
		}
		
		avg = totalage/no;
		System.out.println(avg);
		return avg;
	}
	
	public List<Person> sortPeople(){
		Collections.sort(l, new Comparator<Person>() {

	        public int compare(Person o1, Person o2) {
	            return o2.getLastName().compareTo(o1.getLastName());
	        }
	    });
		
		System.out.println(l.toString());
		
		return l;
	}
	
	
	public List<Person> delete(Person p){
		l.remove(p);
		return l;
	}
	
	//returns the difference between the youngest and the oldest person in days
	public int differenceDays(){
		Collections.sort(l, new Comparator<Person>() {

	        public int compare(Person o1, Person o2) {
	            return o1.getBirthday().compareTo(o2.getBirthday());
	        }
	    });
		
		System.out.println(l.toString());
		
		int num = l.size();
		
		Person p = l.get(0);
		Person p2 = l.get(num-1);
		
		Calendar birthDay = Calendar.getInstance();
		birthDay.set(Calendar.YEAR, p.getBirthday().getYear());
		birthDay.set(Calendar.MONTH, p.getBirthday().getMonthValue());
		birthDay.set(Calendar.DATE, p.getBirthday().getDayOfMonth());
		
		Calendar birthDay2 = Calendar.getInstance();
		birthDay2.set(Calendar.YEAR, p2.getBirthday().getYear());
		birthDay2.set(Calendar.MONTH, p2.getBirthday().getMonthValue());
		birthDay2.set(Calendar.DATE, p2.getBirthday().getDayOfMonth());
		 
		//Get difference between years
		int years = birthDay2.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
		years = years * 365;
		
		//Get difference between months
		int months = birthDay2.get(Calendar.MONTH) - birthDay.get(Calendar.MONTH);
		months = months * 31;
				
		//Get difference between days
		int days = birthDay2.get(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH);
		
		System.out.println(years+months+days);
		return (years + months + days);
	}
	
	public int relationMaleToFemale(){
		int male = 0;
		int female = 0;
		
		for(Person p:l){
			if(p.getSex() == Sex.FEMALE){
				female++;
			}else{
				male++;
			}
		}
		
		int rel = female/male;
		System.out.println("Relation male to female -  1:" + rel);
		return rel;
	}
	
	
	
	//----------------------------
	
		//collector macht stream wieder zu liste
		public void sortName(){
			l = l.stream().sorted((x, y) -> x.getLastName().compareTo(y.getLastName())).collect(Collectors.toList());
		}
		
		
	// -- JAVA8 TESTS
		
		//Durchschnittsalter
		public double averageAgeJava8(){
			double average = l.stream()
					.filter(p -> p.getSex() == Person.Sex.MALE)
					.mapToInt(Person::getAge) // or lambda expression: e -> e.getAge()
					.average() .getAsDouble();
			System.out.println(average);
			return average;
		}
		
		
	//Zweiter Comparator nimmt int nicht
		/*
		//Sortieren nach Name und Alter
		public List<Person> sortNameAge(){
			Comparator<Person> byName = (x, y) -> x.getLastName().compareTo(y.getLastName());

		    Comparator<Person> byAge = (x, y) -> x.getAge().compareTo(y.getAge());

		    
		    l.stream().sorted(byName.thenComparing(byAge))
		            .forEach(e -> System.out.println(e));
			
			
			return l;
		}
*/
		
		//Gruppierung nach Geburtsjahr
		public Map<Object, Long> groupBirthYear(){
			
		    //Map<Integer, Integer> peopleByYear = l.stream().collect(Collectors.groupingBy(p -> p.getBirthYear(), Collectors.counting()));
			Map<Object, Long> counted = l.stream().collect(Collectors.groupingBy(p -> p.getBirthYear(), Collectors.counting()));
		System.out.println(counted);
		
		return counted;
		}
		
		
		//Austreten
		public List<Person> deleteJava8(Person p){
			l.removeIf(x -> x.getLastName().equals(p.getLastName()));
			return l;
		}
		
		
		//Relation
		public void bornInJava8(){
			l.stream().forEach((person) -> System.out.println(person.getLastName() + " was born in " + person.getBirthYear()));
		}
		
		
}
